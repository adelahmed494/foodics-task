<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_uom', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('uom_id')->unsigned()->index()->nullable(false);
            $table->foreign('uom_id')->references('id')->on('uom')->onDelete('cascade');
            $table->bigInteger('ingredient_id')->unsigned()->index()->nullable(false);
            $table->foreign('ingredient_id')->references('id')->on('ingredient')->onDelete('cascade');
            $table->double('multiply_factor',10,2)->unsigned(); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_uom');
    }
};
