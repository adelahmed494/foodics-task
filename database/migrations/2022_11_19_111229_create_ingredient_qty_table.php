<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_qty', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ingredient_id')->unsigned()->index()->nullable(false);
            $table->foreign('ingredient_id')->references('id')->on('ingredient');
            $table->bigInteger('uom_id')->unsigned()->index()->nullable(false);
            $table->foreign('uom_id')->references('id')->on('uom');
            $table->double('qty',5,2)->unsigned()->nullable(false);
            $table->double('unit_cost',5,2)->unsigned()->nullable(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_qty');
    }
};
