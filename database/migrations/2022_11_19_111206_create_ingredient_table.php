<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('default_uom_id')->unsigned()->index()->nullable(false);
            $table->foreign('default_uom_id')->references('id')->on('uom')->nullable(false);
            $table->double('qty',20,2)->unsigned()->default(0);
            $table->double('out_qty',20,2)->unsigned()->default(0);
            $table->timestamp('notified_mail_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient');
    }
};
