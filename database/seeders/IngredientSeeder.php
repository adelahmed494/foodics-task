<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ingredient;
use App\Models\IngredientQty;
use App\Models\IngredientUom;
use App\Models\Uom;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $defult_uom = Uom::where('name', 'g')->first()->id;
        $uom = Uom::where('name', 'kg')->first()->id;

        $ingredient = Ingredient::create([
            'name' => 'Beef',
            'default_uom_id' => $defult_uom,
            'qty' => 20000,
        ]);

        if($ingredient){
            IngredientQty::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'qty' => 10,
                'unit_cost' => 10,
            ]);

            IngredientQty::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'qty' => 10,
                'unit_cost' => 10,
            ]);

            IngredientUom::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'multiply_factor' => 1000,
            ]);
        }


        $ingredient = Ingredient::create([
            'name' => 'Cheese',
            'default_uom_id' => $defult_uom,
            'qty' => 50000,
        ]);

        if($ingredient){
            IngredientQty::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'qty' => 10,
                'unit_cost' => 10,
            ]);

            IngredientQty::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'qty' => 40,
                'unit_cost' => 10,
            ]);

            IngredientUom::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'multiply_factor' => 1000,
            ]);
        }


        $ingredient = Ingredient::create([
            'name' => 'Onion',
            'default_uom_id' => $defult_uom,
            'qty' => 1000,
            
        ]);

        if($ingredient){
            IngredientQty::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $defult_uom,
                'qty' => 800,
                'unit_cost' => 10,
            ]);

            IngredientQty::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $defult_uom,
                'qty' => 200,
                'unit_cost' => 10,
            ]);

            IngredientUom::create([
                'ingredient_id' => $ingredient->id,
                'uom_id' => $uom,
                'multiply_factor' => 1000,
            ]);
        }
    }
}
