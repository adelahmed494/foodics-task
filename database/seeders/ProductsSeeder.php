<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Products;
use App\Models\IngredientQty;
use App\Models\ProductsIngredient;
use App\Models\Ingredient;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $product = Products::create([
            'name' => 'Burger',
            'description' => 'Burger with Beef,Cheese and Onion',
            'price' => 70,
        ]);

        if($product){

            ProductsIngredient::create([
                'product_id' => $product->id,
                'ingredient_id' => Ingredient::where('name', 'Beef')->first()->id,
                'qty' => 150,
            ]);

            ProductsIngredient::create([
                'product_id' => $product->id,
                'ingredient_id' => Ingredient::where('name', 'Cheese')->first()->id,
                'qty' => 30,
            ]);

            ProductsIngredient::create([
                'product_id' => $product->id,
                'ingredient_id' => Ingredient::where('name', 'Onion')->first()->id,
                'qty' => 20,
            ]);
    
        }
    }
}
