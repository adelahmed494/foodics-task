<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Add test user
         \App\Models\User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@foodics.com',
             'password' => Hash::make('P@ssword')
         ]);

         $this->call([
            UomSeeder::class,
            IngredientSeeder::class,
            ProductsSeeder::class,
        ]);
    }
}
