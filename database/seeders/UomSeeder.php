<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Uom::create([
            'id' => 1,
            'name' => 'g'
        ]);

        \App\Models\Uom::create([
            'id' => 2,
            'name' => 'kg'
        ]);
    }
}
