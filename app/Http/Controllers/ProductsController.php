<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Products\ProductsResource;
use App\Models\Products;
use Illuminate\Support\Facades\Cache;

class ProductsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->ValidateRequest($request->all(),Products::$listRules);

        if(Cache::get('products')){
            $products = Cache::get('products')->paginate($request->per_page);
        }else{
            $products = Products::paginate($request->per_page);
        }
        
        return ProductsResource::collection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->ValidateRequest($request->all(),Products::$createRules);

        $product = Products::create([
            'name' => $request->name,
            'description' => isset($request->description) ? $request->description : null,
            'price' => $request->price,
        ]);

        $data = [
            'status' => 'success',
            'message' => 'Product Saved successfully',
            'data' => new ProductsResource($product)
        ];

        $this->updateProductsCache();

        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->ValidateRequest($request->all(),Products::$updateRules);

        $product = Products::find($id);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = isset($request->description) ? $request->description : $product->description;
        $product->save();

        $data = [
            'status' => 'success',
            'message' => 'Product updated successfully',
            'data' => new ItemsResource($product)
        ];

        $this->updateProductsCache();

        return response()->json($data, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function updateProductsCache(){
        if(Cache::get('products')){
            Cache::forget('products');
        }
        Cache::put('products',Products::all());
    }
}
