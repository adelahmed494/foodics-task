<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Orders;
use App\Models\OrdersProducts;
use App\Http\Resources\Orders\OrdersResource;
use App\Events\OrderCreatedEvent;

class OrdersController extends Controller
{
    /**
     * get order method
     * used to get last order created and increase it by 1.
     * to get the new order number.
     */
    public function getOrderNo(){
        $latestOrder = Orders::orderBy('created_at','DESC')->first();
 
        if($latestOrder){
            $orderNo = str_pad((int)$latestOrder->order_no + 1, 8, "0", STR_PAD_LEFT);
        }else{
            $orderNo = str_pad(0 + 1, 8, "0", STR_PAD_LEFT);
        }
        return $orderNo;
    }

    /**
     * get order type
     * used to handle in the future any type og transaction effect.
     * equal sales based on request.
     */
    public function getOrderType(){
        return 'sales';
    }

    /**
     * create new order
     * get order number
     * get order type
     * add order master with type and created by user
     * add product details then fire event to handle stock qty then send email in case qty decreased by 50%
     */
    public function store(Request $request){

        if(!$request->has('type')){
            $request['type'] = $this->getOrderType();
        }

        //get created by user that added order.
        $request['user_id'] = Auth::user()->id;

        $this->ValidateRequest($request->all(),Orders::$createRules);
 
         $request['order_no'] = $this->getOrderNo();
 
         $products_data = array();
 
         foreach($request['products'] as $product){
             $data = array();

             $this->ValidateRequest($product,OrdersProducts::$createRules);

             $data['product_id'] =  $product['product_id'];
             $data['qty'] =  $product['quantity'];
             array_push($products_data,$data); 
         }
 
         $master = Orders::create([
             'type' => $request->type,
             'order_no' => $request->order_no,
             'user_id' => $request->user_id,
             'customer_id' => isset($request->customer_id) ? $request->customer_id : null
         ]);
 
         if(isset($master->id)){
             foreach($products_data as $product){
                 $product['order_id'] = $master->id;
                 $orderProductsData = OrdersProducts::create($product);
             }
         }
         
        
        $reponseData = new OrdersResource($master);
        $data = [
             'status' => 'success',
             'message' => 'Order Saved successfully',
             'data' => new OrdersResource($master)
         ];

         $this->dispatchEvents($master);

         return response()->json($data, 201);
     }

     /**
      * fire event order created.
      */
     private function dispatchEvents($order)
     {
         event(new OrderCreatedEvent($order));
     }

}
