<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Orders\OrdersProductsResource;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "order_no" => $this->order_no,
            "customer_id" =>$this->customer_id,
            "products" => OrdersProductsResource::collection($this->products),
            "created_by" => $this->created_by
        ];
    }
}
