<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\DefaultUomable;
class Ingredient extends Model
{
    use HasFactory,SoftDeletes,DefaultUomable;

    protected $table = 'ingredient';

    protected $fillable = ['name','default_uom_id','qty','out_qty','notified_mail_at'];

    public static $createRules = [
        'name' => 'required|string', 
        'default_uom_id' => 'required|integer|exists:uom,id', 
    ];

    public static $updateRules = [
        'name' => 'required|string',
        'default_uom_id' => 'required|integer|exists:uom,id', 
    ];

    public function checkAvailableQtyLimit(){
        return ($this->out_qty / $this->qty * 100) >= 50;
    }
}
