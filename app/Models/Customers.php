<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'customers';

    protected $fillable = ['name','address','mobile'];

    public static $createRules = [
        'name' => 'required|string', 
        'address' => 'nullable|string', 
        'mobile' => 'nullable|string', 
    ];

    public static $updateRules = [
        'name' => 'required|string', 
        'address' => 'nullable|string', 
        'mobile' => 'nullable|string', 
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];

}
