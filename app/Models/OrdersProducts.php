<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Productable;

class OrdersProducts extends Model
{
    use HasFactory/*,SoftDeletes*/,Productable;

    protected $table = 'orders_products';

    protected $fillable = ['order_id','qty','product_id'];

    public static $createRules = [
        //'order_id' => 'required|integer|exists:orders,id', 
        'product_id' => 'required|integer|exists:products,id', 
        'quantity' => 'required|numeric',
    ];

    public static $updateRules = [
       // 'order_id' => 'required|integer|exists:orders,id', 
        'product_id' => 'required|integer|exists:products,id', 
        'quantity' => 'required|numeric',
    ];

}
