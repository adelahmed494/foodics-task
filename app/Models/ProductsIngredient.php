<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Ingredientable;
use App\Models\Traits\Productable;

class ProductsIngredient extends Model
{
    use HasFactory,SoftDeletes,Ingredientable,Productable;

    protected $table = 'products_ingredient';

    protected $fillable = ['qty','ingredient_id','product_id'];

    public static $createRules = [
        'qty' => 'required|numeric', 
        'product_id' => 'required|integer|exists:products,id', 
        'ingredient_id' => 'required|integer|exists:ingredient,id', 
    ];

    public static $updateRules = [
        'qty' => 'required|numeric', 
        'product_id' => 'required|integer|exists:products,id', 
        'ingredient_id' => 'required|integer|exists:ingredient,id', 
    ];
}
