<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Uomable;
use App\Models\Traits\Ingredientable;

class IngredientQty extends Model
{
    use HasFactory,SoftDeletes,Uomable,Ingredientable;

    protected $table = 'ingredient_qty';

    protected $fillable = ['ingredient_id','uom_id','qty','unit_cost'];

    public static $createRules = [
        'qty' => 'required|numeric', 
        'unit_cost' => 'required|numeric', 
        'uom_id' => 'required|integer|exists:uom,id', 
        'ingredient_id' => 'required|integer|exists:ingredient,id', 
    ];

    public static $updateRules = [
        'qty' => 'required|numeric', 
        'unit_cost' => 'required|numeric', 
        'uom_id' => 'required|integer|exists:uom,id', 
        'ingredient_id' => 'required|integer|exists:ingredient,id', 
    ];
}
