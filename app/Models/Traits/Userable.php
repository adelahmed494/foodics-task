<?php
namespace App\Models\Traits;

Trait Userable
{
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
