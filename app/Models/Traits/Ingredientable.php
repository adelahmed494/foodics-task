<?php
namespace App\Models\Traits;

Trait Ingredientable
{
    public function ingredient(){
        return $this->belongsTo('App\Models\Ingredient', 'ingredient_id', 'id');
    }
}
