<?php
namespace App\Models\Traits;

Trait DefaultUomable
{
    public function uom(){
        return $this->belongsTo('App\Models\Uom', 'default_uom_id', 'id');
    }
}
