<?php
namespace App\Models\Traits;

Trait Uomable
{
    public function uom(){
        return $this->belongsTo('App\Models\Uom', 'uom_id', 'id');
    }
}
