<?php
namespace App\Models\Traits;

Trait Customerable
{
    public function customer(){
        return $this->belongsTo('App\Models\Customers', 'customer_id', 'id');
    }
}
