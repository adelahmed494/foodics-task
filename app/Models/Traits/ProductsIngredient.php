<?php
namespace App\Models\Traits;

Trait ProductsIngredient
{
    public function ProductsIngredient(){
        return $this->hasMany('App\Models\ProductsIngredient', 'product_id', 'id')->with(['ingredient','ingredient.uom']);
    }
}
