<?php
namespace App\Models\Traits;

Trait Productable
{
    public function Product(){
        return $this->belongsTo('App\Models\Products', 'product_id', 'id');
    }
}
