<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\ProductsIngredient;

class Products extends Model
{
    use HasFactory,SoftDeletes,ProductsIngredient;

    protected $table = 'products';

    protected $fillable = ['name','price','description'];

    public static $createRules = [
        'name' => 'required|string', 
        'description' => 'nullable|string', 
        'price' => 'required|numeric', 
    ];

    public static $updateRules = [
        'name' => 'required|string',
        'price' => 'required|numeric',  
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];

}
