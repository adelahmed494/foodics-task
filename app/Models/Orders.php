<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Userable;
use App\Models\Traits\Customerable;

class Orders extends Model
{
    use HasFactory,SoftDeletes,Userable,Customerable;

    protected $table = 'orders';

    protected $fillable = ['type','order_no','customer_id','user_id'];

    public static $createRules = [
        'user_id' => 'required|integer|exists:users,id', 
        'customer_id' => 'nullable|integer|exists:customers,id', 
        'type' => 'required|in:sales,return',
        'products' => 'required',
    ];

    public static $updateRules = [
        'user_id' => 'required|integer|exists:users,id',  
        'customer_id' => 'nullable|integer|exists:customers,id', 
        'type' => 'required|in:sales,return',
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];

    public function products(){
        return $this->hasMany('App\Models\OrdersProducts', 'order_id', 'id');
    }
}
