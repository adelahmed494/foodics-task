<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Uomable;
use App\Models\Traits\Ingredientable;
class IngredientUom extends Model
{
    use HasFactory,SoftDeletes,Uomable,Ingredientable;

    protected $fillable = ['ingredient_id','uom_id','multiply_factor'];

    protected $table = 'ingredient_uom';

    public static $createRules = [
        'multiply_factor' => 'required|numeric', 
        'uom_id' => 'required|integer|exists:uom,id', 
        'ingredient_id' => 'required|integer|exists:ingredient,id', 
    ];

    public static $updateRules = [
        'multiply_factor' => 'required|numeric', 
        'uom_id' => 'required|integer|exists:uom,id', 
        'ingredient_id' => 'required|integer|exists:ingredient,id',
    ];
}
