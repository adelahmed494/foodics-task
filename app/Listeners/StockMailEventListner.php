<?php

namespace App\Listeners;

use App\Events\StockMailEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\StockQtyEmailMailable;
use Illuminate\Support\Facades\Mail;

class StockMailEventListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StockMailEvent  $event
     * @return void
     */
    public function handle(StockMailEvent $event)
    {
        Mail::to(env('MERCHANT_MAIL','ahmedadelomar2831993@gmail.com'))->send(new StockQtyEmailMailable());
        $event->ingredient->notified_mail_at = now();
        $event->ingredient->save();
    }
}
