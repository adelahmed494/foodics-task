<?php

namespace App\Listeners;

use App\Events\OrderCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\StockMailEvent;

class OrderCreatedEventListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderCreatedEvent  $event
     * @return void
     */
    public function handle(OrderCreatedEvent $event)
    {
        if(isset($event->order->products)){
            foreach($event->order->products as $orderProducts){
                if(isset($orderProducts->Product->ProductsIngredient)){
                    foreach($orderProducts->Product->ProductsIngredient as $ingredient){

                        if($event->order->type == 'sales')
                            $ingredient->ingredient->out_qty  += ($ingredient->qty * $orderProducts->qty);
                        else 
                            $ingredient->ingredient->out_qty  -= ($ingredient->qty * $orderProducts->qty);

                        $ingredient->ingredient->save();

                        if($ingredient->ingredient->checkAvailableQtyLimit() && is_null($ingredient->ingredient->notified_mail_at)){
                            event(new StockMailEvent($ingredient->ingredient));
                        }   

                    }
                }
                
            }
        }
    }
}
