<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Products;

class OrdersTest extends TestCase
{
    /**
     * A basic feature test Order Without Auth Token .
     * used to test order without Authorization
     */

    public function testOrderWithoutAuthToken()
    {
        $payload = [
                        "products"=>[
                                    [
                                        "product_id" => 1,
                                        "quantity" => 1
                                    ]
                                ]
                    ];

        $this->json('post', 'api/order', $payload)
        ->assertStatus(401);
    }

    /**
     * A basic feature test Order Is Created Successfully .
     * used to test order created successfully
     */
    public function testOrderIsCreatedSuccessfully()
    { 
        $userData = ['email' => 'test@foodics.com','password' => 'P@ssword'];

        $response = $this->json('post', 'api/login', $userData)
        ->assertStatus(200);

        $token = $response['authorisation']['token'];

        $payload = [
                        "products"=>[
                                    [
                                        "product_id" => 1,
                                        "quantity" => 1
                                    ]
                                ]
                    ];
       
        $response = $this->withHeaders([
            'Authorization' => 'Bearer {$token}',
        ])->json('post', 'api/order', $payload)
        ->assertStatus(Response::HTTP_CREATED);
    }

    /**
     * A basic feature test Order Without ProductId .
     * used to validation of orders.
     */
    public function testOrderWithoutProductId()
    {
        $userData = ['email' => 'test@foodics.com','password' => 'P@ssword'];

        $response = $this->json('post', 'api/login', $userData)
        ->assertStatus(200);
        $token = $response['authorisation']['token'];

        $payload = [
                        "products"=>[
                                    [
                                        "quantity" => 1
                                    ]
                                ]
                    ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer {$token}',
        ])->json('post', 'api/order', $payload)
        ->assertStatus(200);
    }

    /**
     * A basic feature test Order Without ProductId .
     * used to validation of orders.
     */

    public function testOrderWithoutQuantity()
    {
        $userData = ['email' => 'test@foodics.com','password' => 'P@ssword'];

        $response = $this->json('post', 'api/login', $userData)
        ->assertStatus(200);
        $token = $response['authorisation']['token'];

        $payload = [
                        "products"=>[
                                    [
                                        "product_id" => 1
                                    ]
                                ]
                    ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer {$token}',
        ])->json('post', 'api/order', $payload)
        ->assertStatus(200);
    }

    /**
     * A basic feature test Order Update Stock Successfully .
     * used to validation stock of ingredient changed.
     */

    public function testOrderUpdateStockSuccessfully()
    {
        $userData = ['email' => 'test@foodics.com','password' => 'P@ssword'];

        $response = $this->json('post', 'api/login', $userData)
        ->assertStatus(200);
        $token = $response['authorisation']['token'];

        $product = Products::find(1);
        $beforeCreateArray = array();
        $afterCreateArray = array();

        foreach($product->ProductsIngredient as $ingredient){
            $beforeCreateArray[$ingredient->ingredient->name] = $ingredient->ingredient->out_qty;
        }

        $payload = [
                        "products"=>[
                                    [
                                        "product_id" => 1,
                                        "quantity" => 1
                                    ]
                                ]
                    ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer {$token}',
        ])->json('post', 'api/order', $payload)
        ->assertStatus(Response::HTTP_CREATED);

        $product = Products::find(1);
        foreach($product->ProductsIngredient as $ingredient){
            $afterCreateArray[$ingredient->ingredient->name] = $ingredient->ingredient->out_qty;

            if($afterCreateArray[$ingredient->ingredient->name] == $beforeCreateArray[$ingredient->ingredient->name]){
                return 'failed update stock qty';
            }
        }
    }

}
